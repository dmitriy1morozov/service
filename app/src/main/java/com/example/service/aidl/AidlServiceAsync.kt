package com.example.service.aidl

import android.app.Service
import android.content.Intent
import android.os.Handler
import android.os.IBinder
import android.util.Log
import com.example.service.AsyncCallback
import com.example.service.ICalculatorAsync
import com.example.service.ICalculatorSync

class AidlServiceAsync : Service() {

    override fun onBind(intent: Intent): IBinder {
        Log.d(TAG, "onBind: ")

        return object : ICalculatorAsync.Stub() {
            override fun sum(first: Int, second: Int, callback: AsyncCallback?) {
                Log.d(
                    TAG,
                    "sum() called with: first = $first, second = $second, callback = $callback, thread=${Thread.currentThread().name}"
                )

                Thread {
                    Thread.sleep(5000)
                    callback?.onSuccess(Sum(first + second))
                }.start()
            }
        }
    }

    companion object {
        val TAG: String = AidlServiceAsync::class.java.simpleName
    }
}