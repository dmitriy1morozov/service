package com.example.service.aidl

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import com.example.service.ICalculatorSync

class AidlServiceSync : Service() {

    override fun onBind(intent: Intent): IBinder {
        Log.d(TAG, "onBind: ")

        return object : ICalculatorSync.Stub() {
            override fun sum(first: Int, second: Int): Sum {
                Log.d(
                    TAG,
                    "sum() called with: first = $first, second = $second in thread=${Thread.currentThread().name}"
                )
                return Sum(first + second)
            }
        }
    }

    companion object {
        val TAG: String = AidlServiceSync::class.java.simpleName
    }
}