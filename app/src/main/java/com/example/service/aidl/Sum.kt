package com.example.service.aidl

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Sum(val sum: Int) : Parcelable
