package com.example.service.aidl

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.service.AsyncCallback
import com.example.service.ICalculatorAsync
import com.example.service.ICalculatorSync
import com.example.service.R

class AidlActivity : AppCompatActivity() {

    private var calculatorSync: ICalculatorSync? = null
    private var calculatorAsync: ICalculatorAsync? = null


    private val connectionSync = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, binder: IBinder?) {
            Log.d(TAG, "onServiceConnected() called with: name = $name, binder = $binder")
            calculatorSync = ICalculatorSync.Stub.asInterface(binder)
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            Log.d(TAG, "onServiceDisconnected() called with: name = $name")
            calculatorSync = null
        }
    }

    private val connectionAsync = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, binder: IBinder?) {
            Log.d(TAG, "onServiceConnected() called with: name = $name, binder = $binder")
            calculatorAsync = ICalculatorAsync.Stub.asInterface(binder)
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            Log.d(TAG, "onServiceDisconnected() called with: name = $name")
            calculatorAsync = null
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_aidl)

        findViewById<Button>(R.id.btnSumSync).setOnClickListener {
            val sum = calculatorSync?.sum(1, 3)
            Log.d(TAG, "Sum = ${sum?.sum}, thread=${Thread.currentThread().name}")
        }

        findViewById<Button>(R.id.btnSumAsync).setOnClickListener {
            calculatorAsync?.sum(1, 3, object : AsyncCallback.Stub() {
                override fun onSuccess(sum: Sum?) {
                    Log.d(TAG, "Sum = ${sum?.sum}, thread=${Thread.currentThread().name}")
                }
            })
        }
    }

    override fun onStart() {
        Log.d(TAG, "onStart() called")
        super.onStart()
        bindService(
            getExplicitIntent("com.example.service.AIDL_SYNC"),
            connectionSync,
            BIND_AUTO_CREATE
        )
        bindService(
            getExplicitIntent("com.example.service.AIDL_ASYNC"),
            connectionAsync,
            BIND_AUTO_CREATE
        )
    }

    private fun getExplicitIntent(actionName: String): Intent {
        val intent = Intent(actionName)
        val services = packageManager.queryIntentServices(intent, 0)
        if (services.isEmpty()) {
            throw IllegalStateException("Connection to AIDL service is not established")
        }

        return Intent(intent).apply {
            val resolveInfo = services[0]
            val packageName = resolveInfo.serviceInfo.packageName
            val className = resolveInfo.serviceInfo.name
            component = ComponentName(packageName, className)
        }
    }

    override fun onStop() {
        Log.d(TAG, "onStop() called")
        super.onStop()
        unbindService(connectionSync)
        unbindService(connectionAsync)
    }

    companion object {
        val TAG: String = AidlActivity::class.java.simpleName

        fun start(context: Context) {
            context.startActivity(Intent(context, AidlActivity::class.java))
        }
    }
}