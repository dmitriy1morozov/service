package com.example.service.bound

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log
import java.lang.ref.WeakReference
import java.util.*


class MyBoundService : Service() {

    private val listeners: MutableList<WeakReference<(data: String) -> Unit>> = mutableListOf()

    private val binder = LocalBinder()

    var timer: Timer = Timer()
    var task: TimerTask? = null
    var interval = 1000L

    override fun onCreate() {
        Log.d(TAG, "onCreate() called")
        super.onCreate()
        timer = Timer()
        schedule()
    }

    override fun onDestroy() {
        Log.d(TAG, "onDestroy() called")
        task?.cancel()
        super.onDestroy()
    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d(
            TAG,
            "onStartCommand() called with: intent = $intent, flags = $flags, startId = $startId"
        )
        return super.onStartCommand(intent, flags, startId)
//        return Service.START_STICKY - OS will try to restore the service after its crash/termination  with Intent=null
//        return Service.START_REDELIVER_INTENT - OS will try to restore the service after its crash/termination with the same Intent
//        return Service.START_NOT_STICKY - OS will NOT try to restore the service after its crash/termination
    }

    override fun onBind(intent: Intent): IBinder {
        Log.d(TAG, "onBind() called with: intent = $intent")
        return binder
    }

    override fun onUnbind(intent: Intent?): Boolean {
        Log.d(TAG, "onUnbind() called with: intent = $intent")
        return true
    }

    override fun onRebind(intent: Intent?) {
        Log.d(TAG, "onRebind() called with: intent = $intent")
        super.onRebind(intent)
    }


    private fun schedule() {
        Log.d(TAG, "schedule() called")

        task?.cancel()
        if (interval > 0) {
            task = object : TimerTask() {
                override fun run() {
                    Log.d(TAG, "run")

                    listeners.forEach {
                        it.get()?.invoke("Data via LAMBDA")
                    }
                }
            }
            timer.schedule(task, 1000, interval)
        }
    }

    fun increaseInterval(): Long {
        Log.d(TAG, "increaseInterval() called")
        interval += 500
        schedule()
        return interval
    }

    fun decreaseInterval(): Long {
        Log.d(TAG, "decreaseInterval() called")
        interval -= 500
        interval.coerceAtLeast(500)
        schedule()
        return interval
    }

    fun addListener(listener: (String) -> Unit) {
        Log.d(TAG, "addListenerFun() called with: listener = $listener")
        listeners.add(WeakReference(listener))
    }

    inner class LocalBinder : Binder() {
        fun getService(): MyBoundService = this@MyBoundService
    }

    companion object {
        val TAG: String = MyBoundService::class.java.simpleName
    }
}