package com.example.service.bound

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.service.R

class BoundActivity : AppCompatActivity() {

    private var isBound = false
    private val serviceIntent by lazy { Intent(this, MyBoundService::class.java) }
    private var service: MyBoundService? = null

    private val serviceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(component: ComponentName?, binder: IBinder?) {
            Log.d(
                TAG,
                "onServiceConnected() called with: component = $component, binder = $binder"
            )

            binder as MyBoundService.LocalBinder
            service = binder.getService().apply {
                addListener { data ->
                    Log.d(TAG, "onDataReceived in LAMBDA: $data")
                }
            }
        }

        override fun onServiceDisconnected(component: ComponentName?) {
            Log.d(TAG, "onServiceDisconnected() called with: component = $component")
            service = null
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(TAG, "onCreate: ")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bound)

        findViewById<Button>(R.id.btnBind).setOnClickListener(::bindService)
        findViewById<Button>(R.id.btnStart).setOnClickListener(::startService)
        findViewById<Button>(R.id.btnStop).setOnClickListener(::stopService)
        findViewById<Button>(R.id.btnUnbind).setOnClickListener(::unbindService)
        findViewById<Button>(R.id.btnIncrease).setOnClickListener(::increase)
        findViewById<Button>(R.id.btnDecrease).setOnClickListener(::decrease)
    }

    override fun onStart() {
        Log.d(TAG, "onStart() called")
        super.onStart()
        bindService(null)
    }

    override fun onStop() {
        Log.d(TAG, "onStop: ")
        super.onStop()
        unbindService(null)
    }

    private fun bindService(view: View?) {
        Log.d(TAG, "bindService: isRunning = ${service != null}, isBound = $isBound")
        isBound = bindService(serviceIntent, serviceConnection, BIND_AUTO_CREATE)
    }

    private fun unbindService(view: View?) {
        Log.d(TAG, "unbindService: isRunning = ${service != null}, isBound = $isBound")

        if (isBound) {
            unbindService(serviceConnection)
            isBound = false
        }
    }

    private fun startService(view: View?) {
        Log.d(TAG, "startService: isRunning = ${service != null}, isBound = $isBound")
        startService(serviceIntent)
    }

    private fun stopService(view: View?) {
        Log.d(TAG, "stopService: isRunning = ${service != null}, isBound = $isBound")
        stopService(serviceIntent)
    }

    private fun increase(view: View?) {
        Log.d(TAG, "increase: isRunning = ${service != null}, isBound = $isBound")

        if (isBound) {
            val interval = service?.increaseInterval()
            Log.d(TAG, "increase: interval to $interval")
        }
    }

    private fun decrease(view: View?) {
        Log.d(TAG, "decrease: isRunning = ${service != null}, isBound = $isBound")

        if (isBound) {
            val interval = service?.decreaseInterval()
            Log.d(TAG, "decrease: interval to $interval")
        }
    }

    companion object {
        val TAG: String = BoundActivity::class.java.simpleName

        fun start(context: Context) {
            context.startActivity(Intent(context, BoundActivity::class.java))
        }
    }
}