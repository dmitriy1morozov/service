package com.example.service.wrappers.broadcastreceiver

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log
import com.example.service.wrappers.broadcastreceiver.BroadcastReceiverActivity.Companion.BROADCAST_ACTION

class BroadcastService : Service() {

    private val binder = LocalBinder()

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d(TAG, "onStartCommand: ")

        Thread {
            Thread.sleep(3000)
            val broadcastIntent = Intent(BROADCAST_ACTION)
            // put data into intent
            sendBroadcast(broadcastIntent)
            stopSelf()
        }.start()
        return super.onStartCommand(intent, flags, startId)
    }


    override fun onBind(intent: Intent?): IBinder = binder

    inner class LocalBinder : Binder() {
        fun getService(): BroadcastService = this@BroadcastService
    }

    companion object {
        val TAG: String = BroadcastService::class.java.simpleName
    }
}