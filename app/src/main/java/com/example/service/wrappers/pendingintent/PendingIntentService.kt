package com.example.service.wrappers.pendingintent

import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log
import com.example.service.wrappers.pendingintent.PendingIntentActivity.Companion.PARAM_PENDING_INTENT
import com.example.service.wrappers.pendingintent.PendingIntentActivity.Companion.STATUS_FINISH
import com.example.service.wrappers.pendingintent.PendingIntentActivity.Companion.STATUS_START

class PendingIntentService : Service() {

    private val binder = LocalBinder()

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d(TAG, "onStartCommand: ")
        val pendingIntent = intent?.getParcelableExtra<PendingIntent>(PARAM_PENDING_INTENT)

        Thread {
            // send first result
            pendingIntent?.send(STATUS_START)
            // do some work
            Thread.sleep(3000)
            // send second result
            val resultIntent = Intent().apply {
                putExtra("some_data", 123)
            }
            pendingIntent?.send(this, STATUS_FINISH, resultIntent)
            stopSelf()
        }.start()
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onBind(intent: Intent?): IBinder = binder

    inner class LocalBinder : Binder() {
        fun getService(): PendingIntentService = this@PendingIntentService
    }

    companion object {
        val TAG: String = PendingIntentService::class.java.simpleName
    }
}