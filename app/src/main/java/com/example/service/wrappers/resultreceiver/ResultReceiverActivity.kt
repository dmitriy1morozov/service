package com.example.service.wrappers.resultreceiver

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.ResultReceiver
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.service.R

class ResultReceiverActivity : AppCompatActivity() {

    private lateinit var receiver: ResultReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result_receiver)

        receiver = object : ResultReceiver(Handler()) {
            override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {
                // Get data from bundle according to resultCode
                if (resultCode == RESULT_CODE) {
                    Log.d(TAG, "onReceiveResult: thread=${Thread.currentThread().name}")
                }
                super.onReceiveResult(resultCode, resultData)
            }
        }
        val myIntent = Intent(this, ResultReceiverService::class.java).apply {
            putExtra(PARAM_RECEIVER, receiver)
        }

        findViewById<Button>(R.id.btnSend).setOnClickListener {
            startService(myIntent)
        }
    }

    companion object {
        val TAG: String = ResultReceiverActivity::class.java.simpleName
        const val RESULT_CODE = 1001
        const val PARAM_RECEIVER = "param_receiver"

        fun start(context: Context) {
            context.startActivity(Intent(context, ResultReceiverActivity::class.java))
        }
    }
}