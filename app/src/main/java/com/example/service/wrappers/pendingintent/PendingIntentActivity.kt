package com.example.service.wrappers.pendingintent

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.service.R

class PendingIntentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pending_intent)

        findViewById<Button>(R.id.btnSend).setOnClickListener {
            // put some default data to supply in the result, which may be modified by the sender.
            val requestIntent = Intent()
            val pendingIntent = createPendingResult(REQUEST_CODE, requestIntent, 0)
            val serviceIntent = Intent(this, PendingIntentService::class.java).apply {
                putExtra(PARAM_PENDING_INTENT, pendingIntent)
            }
            startService(serviceIntent)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CODE) {
            Log.d(TAG, "onActivityResult: resultCode=$resultCode")
        }
    }

    companion object {
        val TAG: String = PendingIntentActivity::class.java.simpleName
        const val REQUEST_CODE = 10001
        const val PARAM_PENDING_INTENT = "pending_intent"
        const val STATUS_START = 101
        const val STATUS_FINISH = 102

        fun start(context: Context) {
            context.startActivity(Intent(context, PendingIntentActivity::class.java))
        }
    }
}