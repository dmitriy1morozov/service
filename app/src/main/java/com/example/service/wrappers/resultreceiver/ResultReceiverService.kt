package com.example.service.wrappers.resultreceiver

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.Bundle
import android.os.IBinder
import android.os.ResultReceiver
import android.util.Log
import com.example.service.wrappers.resultreceiver.ResultReceiverActivity.Companion.PARAM_RECEIVER
import com.example.service.wrappers.resultreceiver.ResultReceiverActivity.Companion.RESULT_CODE

class ResultReceiverService : Service() {

    private val binder = LocalBinder()

    override fun onBind(intent: Intent?): IBinder = binder

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d(TAG, "onStartCommand: ")

        Thread {
            Thread.sleep(3000)
            val receiver = intent?.getParcelableExtra<ResultReceiver>(PARAM_RECEIVER)
            // processing
            val bundle = Bundle()
            // put data and send result
            receiver?.send(RESULT_CODE, bundle)
            stopSelf()
        }.start()

        return super.onStartCommand(intent, flags, startId)
    }

    inner class LocalBinder : Binder() {
        fun getService(): ResultReceiverService = this@ResultReceiverService
    }

    companion object {
        val TAG: String = ResultReceiverService::class.java.simpleName
    }
}