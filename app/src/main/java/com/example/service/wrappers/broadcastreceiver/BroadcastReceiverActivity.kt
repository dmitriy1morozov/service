package com.example.service.wrappers.broadcastreceiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.service.R

class BroadcastReceiverActivity : AppCompatActivity() {

    // You may use LocalBroadcastReceiver instead. It is more preferable way
    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            Log.d(
                TAG,
                "onReceive() called with: context = $context, intent = $intent, thread=${Thread.currentThread().name}"
            )
            // Get data from intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_broadcast_receiver)

        findViewById<Button>(R.id.btnSend).setOnClickListener {
            startService(Intent(this, BroadcastService::class.java))
        }
    }

    override fun onStart() {
        super.onStart()
        registerReceiver(receiver, IntentFilter(BROADCAST_ACTION))
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(receiver)
    }

    companion object {
        val TAG: String = BroadcastReceiverActivity::class.java.simpleName
        const val BROADCAST_ACTION = "com.example.service.wrappers.broadcastreceiver.ACTION"

        fun start(context: Context) {
            context.startActivity(Intent(context, BroadcastReceiverActivity::class.java))
        }
    }
}