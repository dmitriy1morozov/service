package com.example.service

import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.service.aidl.AidlActivity
import com.example.service.bound.BoundActivity
import com.example.service.hamer.HamerActivity
import com.example.service.wrappers.broadcastreceiver.BroadcastReceiverActivity
import com.example.service.wrappers.pendingintent.PendingIntentActivity
import com.example.service.wrappers.resultreceiver.ResultReceiverActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(TAG, "onCreate: ")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<Button>(R.id.btnBound).setOnClickListener {
            BoundActivity.start(this)
        }

        findViewById<Button>(R.id.btnHamer).setOnClickListener {
            HamerActivity.start(this)
        }

        findViewById<Button>(R.id.btnAidl).setOnClickListener {
            AidlActivity.start(this)
        }

        findViewById<Button>(R.id.btnBroadcastReceiver).setOnClickListener {
            BroadcastReceiverActivity.start(this)
        }

        findViewById<Button>(R.id.btnPendingIntent).setOnClickListener {
            PendingIntentActivity.start(this)
        }

        findViewById<Button>(R.id.btnResultReceiver).setOnClickListener {
            ResultReceiverActivity.start(this)
        }
    }


    companion object {
        val TAG: String = MainActivity::class.java.simpleName
    }
}