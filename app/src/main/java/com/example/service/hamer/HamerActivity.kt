package com.example.service.hamer

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.*
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.service.R

class HamerActivity : AppCompatActivity() {

    private var serviceMessenger: Messenger? = null
    private val clientMessenger = Messenger(object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            Log.d(
                TAG,
                "handleMessage CLIENT: arg1=${msg.arg1} on thread: ${Thread.currentThread().name}"
            )
        }
    })

    private val connection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, binder: IBinder?) {
            serviceMessenger = Messenger(binder)
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            serviceMessenger = null
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(TAG, "onCreate() called with: savedInstanceState = $savedInstanceState")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hamer)

        findViewById<Button>(R.id.btnStart).setOnClickListener(::startService)
        findViewById<Button>(R.id.btnSend).setOnClickListener(::sendData)
    }


    private fun startService(view: View?) {
        val intent = Intent(this, HamerService::class.java).apply {
            putExtra("messenger", clientMessenger)
        }

        startService(intent)
    }

    private fun sendData(view: View?) {
        val message = Message.obtain()
        message.replyTo = clientMessenger
        message.arg1 = 100

        try {
            serviceMessenger?.send(message)
        } catch (remoteException: RemoteException) {
            remoteException.printStackTrace()
        }
    }

    override fun onStart() {
        Log.d(TAG, "onStart() called")
        super.onStart()
        val intent = Intent(this, HamerService::class.java)
        bindService(intent, connection, BIND_AUTO_CREATE)
    }

    override fun onStop() {
        Log.d(TAG, "onStop() called")
        super.onStop()
        unbindService(connection)
    }

    companion object {
        val TAG: String = HamerActivity::class.java.simpleName

        fun start(context: Context) {
            context.startActivity(Intent(context, HamerActivity::class.java))
        }
    }
}