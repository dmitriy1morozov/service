package com.example.service.hamer

import android.app.Service
import android.content.Intent
import android.os.*
import android.util.Log

class HamerService : Service() {

    private lateinit var handlerThread: HandlerThread
    private lateinit var messenger: Messenger
    private lateinit var handler: Handler

    override fun onCreate() {
        Log.d(TAG, "onCreate() called")
        super.onCreate()
        handlerThread = HandlerThread("Background HandlerThread")
        handlerThread.start()

        handler = Handler(handlerThread.looper) { msg ->
            Log.d(
                TAG,
                "handleMessage SERVICE: arg1=${msg.arg1} in HamerService on thread: ${Thread.currentThread().name}"
            )
            val newMessage = Message.obtain()

            newMessage.arg1 = 200
            msg.replyTo.send(newMessage)
            return@Handler false
        }

        messenger = Messenger(handler)
    }

    // One more channel of communication. Spin up by sending messenger in parcelable in intent
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d(TAG, "onStartCommand: ")

        val client = intent?.getParcelableExtra<Messenger>("messenger")
        val message = Message.obtain(handler)
        message.replyTo = client
        message.sendToTarget()

        return START_REDELIVER_INTENT
    }

    override fun onBind(intent: Intent): IBinder {
        Log.d(TAG, "onBind() called with: intent = $intent")
        return messenger.binder
    }

    override fun onUnbind(intent: Intent?): Boolean {
        Log.d(TAG, "onUnbind() called with: intent = $intent")
        handlerThread.quit()
        return true
    }

    override fun onDestroy() {
        Log.d(TAG, "onDestroy() called")
        super.onDestroy()
    }

    companion object {
        val TAG: String = HamerService::class.java.simpleName
    }
}