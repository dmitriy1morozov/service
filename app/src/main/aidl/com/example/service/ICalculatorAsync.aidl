package com.example.service;

import com.example.service.aidl.Sum;
import com.example.service.AsyncCallback;

interface ICalculatorAsync {

    void sum(int first, int second, AsyncCallback callback);
}