package com.example.service;

import com.example.service.aidl.Sum;

interface AsyncCallback {
    void onSuccess(in Sum sum);
}