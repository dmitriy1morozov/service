package com.example.service;

import com.example.service.aidl.Sum;

interface ICalculatorSync {

    Sum sum(int first, int second);
}