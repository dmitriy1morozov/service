This is a sample project showing Service basics and communication between Activity and Service 

![](/Overview.png)

## Service Category:
- **StartService**
- **BoundService**

## Service start flags:
- **START_STICKY** - tells the OS to recreate the service after it has enough memory and call onStartCommand() again with a null intent.
- **START_NOT_STICKY** - tells the OS to not bother recreating the service again.
- **START_REDELIVER_INTENT** - tells the OS to recreate the service and redeliver the same intent to onStartCommand() once recreated.

## Types of Services:
- **Foreground Services**: A foreground service performs operation that is noticeable to the user. It must provide a notification, which is placed under the "Ongoing" heading, which means that the notification cannot be dismissed unless the service is either stopped or removed from the foreground. (e.g. audio app using foreground service to play audio track). Use startForegroundService() class to start this service.
- **Background Services**: A background service performs an operation that isn't directly noticed by the user. (e.g. app using background service to compact its storage)
- **Bound Services**: better choice for more complex two-way interactions between activities and services. It allows the launching component to interact with, and receive results from the service. Started service does not generally return results or permit interaction with the component that launched it (requires complex programming).

###### Bound Service:
- **Bound service** - Allows other components to bind it to the bound service to get some functionalities. One service can have multiple clients. Only when the last component unbounds, the service is stopped. So, it's important to stop the service by overriding the onTaskRemoved() and then calling the stopSelf().
- **Local bound** - client and service in same application. There is 2 way you can bound a service. One is after starting a service and then binding it or by just binding it directly. If you do allow your service to be started and bound, then when the service has been started, the system does not destroy the service when all clients unbind. Instead, you must explicitly stop the service by calling stopSelf() or stopService().
- **Remote bound** - AIDL - to connect to the client.

###### Why use Bound Service?
It allows components (such as activities) to bind to the service, send requests, receive responses, and perform interprocess communication (IPC). A bound service typically lives only while it serves another application component and does not run in the background indefinitely.

###### There are 3 ways you can define the interface:
- **Extending the binder class** - If your service is private to your own application and runs in the same process as the client
- **Using a Messenger** - If you want to work across different processes, use Messenger. It creates a queue of all the client requests in a single thread, so the service receives requests one at a time.
- **Using AIDL** - If you want your service to handle multiple requests simultaneously, then use AIDL. Make sure your service is thread-safe and capable of multi-threading.
